window.__imported__ = window.__imported__ || {};
window.__imported__["categories/layers.json.js"] = [
	{
		"id": 479,
		"name": "categoriesAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 416,
				"name": "Categories",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 467,
						"name": "Health",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Health.png",
							"frame": {
								"x": 36,
								"y": 752,
								"width": 148,
								"height": 184
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "600473138"
					},
					{
						"id": 463,
						"name": "Coffee",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Coffee.png",
							"frame": {
								"x": 264,
								"y": 750,
								"width": 234,
								"height": 185
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "169598265"
					},
					{
						"id": 459,
						"name": "Beer",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Beer.png",
							"frame": {
								"x": 602,
								"y": 751,
								"width": 88,
								"height": 184
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1708386798"
					},
					{
						"id": 397,
						"name": "Restaurants",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Restaurants.png",
							"frame": {
								"x": 287,
								"y": 452,
								"width": 191,
								"height": 173
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "989104728"
					},
					{
						"id": 402,
						"name": "Drinks",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Drinks.png",
							"frame": {
								"x": 591,
								"y": 461,
								"width": 108,
								"height": 171
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "805393619"
					},
					{
						"id": 407,
						"name": "Search",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Search.png",
							"frame": {
								"x": 83,
								"y": 247,
								"width": 619,
								"height": 105
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "357330888"
					},
					{
						"id": 390,
						"name": "Nearby",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Nearby.png",
							"frame": {
								"x": 38,
								"y": 428,
								"width": 132,
								"height": 204
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1605812998"
					}
				],
				"modification": "856157003"
			},
			{
				"id": 6,
				"name": "Heading",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 43,
						"name": "activeTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/activeTab.png",
							"frame": {
								"x": 0,
								"y": 168,
								"width": 375,
								"height": 11
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96449660"
					},
					{
						"id": 37,
						"name": "categoriesTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/categoriesTab.png",
							"frame": {
								"x": 458,
								"y": 139,
								"width": 133,
								"height": 28
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96449635"
					},
					{
						"id": 31,
						"name": "peopleTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/peopleTab.png",
							"frame": {
								"x": 150,
								"y": 139,
								"width": 83,
								"height": 28
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96449568"
					},
					{
						"id": 10,
						"name": "appTitle",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/appTitle.png",
							"frame": {
								"x": 36,
								"y": 60,
								"width": 314,
								"height": 44
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96419992"
					},
					{
						"id": 26,
						"name": "addFriends",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/addFriends.png",
							"frame": {
								"x": 424,
								"y": 42,
								"width": 144,
								"height": 78
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96360232"
					},
					{
						"id": 28,
						"name": "Settings",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Settings.png",
							"frame": {
								"x": 608,
								"y": 48,
								"width": 108,
								"height": 72
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96360229"
					},
					{
						"id": 305,
						"name": "dividerTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/dividerTab.png",
							"frame": {
								"x": 0,
								"y": 173,
								"width": 750,
								"height": 6
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "96360226"
					},
					{
						"id": 307,
						"name": "background",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/background.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 750,
								"height": 188
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "280203057"
					}
				],
				"modification": "916546875"
			},
			{
				"id": 427,
				"name": "BackgroundC",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundC.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 750,
						"height": 1331
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "96360198"
			}
		],
		"modification": "1253704938"
	}
]