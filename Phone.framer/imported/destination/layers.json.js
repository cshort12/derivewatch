window.__imported__ = window.__imported__ || {};
window.__imported__["destination/layers.json.js"] = [
	{
		"id": 471,
		"name": "destinationAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 437,
				"name": "Destination",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 455,
						"name": "Result",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Result.png",
							"frame": {
								"x": 0,
								"y": 659,
								"width": 750,
								"height": 58
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1940100744"
					},
					{
						"id": 425,
						"name": "cafeYesterday",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/cafeYesterday.png",
							"frame": {
								"x": 16,
								"y": 914,
								"width": 717,
								"height": 148
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1669246485"
					},
					{
						"id": 423,
						"name": "philz",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/philz.png",
							"frame": {
								"x": 44,
								"y": 1095,
								"width": 578,
								"height": 127
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "169509358"
					},
					{
						"id": 421,
						"name": "local123",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/local123.png",
							"frame": {
								"x": 16,
								"y": 738,
								"width": 717,
								"height": 147
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1373237987"
					},
					{
						"id": 411,
						"name": "map",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/map.png",
							"frame": {
								"x": 0,
								"y": 188,
								"width": 750,
								"height": 453
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1223635320"
					},
					{
						"id": 6,
						"name": "Heading",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 31,
								"name": "destTab",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/destTab.png",
									"frame": {
										"x": 65,
										"y": 133,
										"width": 178,
										"height": 29
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223635315"
							},
							{
								"id": 450,
								"name": "Back",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Back.png",
									"frame": {
										"x": 36,
										"y": 38,
										"width": 47,
										"height": 82
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223635221"
							},
							{
								"id": 10,
								"name": "appTitle",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/appTitle.png",
									"frame": {
										"x": 121,
										"y": 60,
										"width": 314,
										"height": 44
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223635196"
							},
							{
								"id": 28,
								"name": "Settings",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Settings.png",
									"frame": {
										"x": 608,
										"y": 48,
										"width": 108,
										"height": 72
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223545976"
							},
							{
								"id": 305,
								"name": "dividerTab",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/dividerTab.png",
									"frame": {
										"x": 0,
										"y": 173,
										"width": 750,
										"height": 6
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223545973"
							},
							{
								"id": 307,
								"name": "background",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/background.png",
									"frame": {
										"x": 0,
										"y": 0,
										"width": 750,
										"height": 188
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1223545949"
							}
						],
						"modification": "1866349647"
					}
				],
				"modification": "1810397697"
			},
			{
				"id": 441,
				"name": "BackgroundD",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundD.png",
					"frame": {
						"x": 0,
						"y": 12,
						"width": 750,
						"height": 1319
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1223545944"
			}
		],
		"modification": "406806483"
	}
]