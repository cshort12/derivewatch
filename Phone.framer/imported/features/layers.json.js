window.__imported__ = window.__imported__ || {};
window.__imported__["features/layers.json.js"] = [
	{
		"id": 535,
		"name": "featuresAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 462,
				"name": "Features",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 481,
						"name": "invite",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/invite.png",
							"frame": {
								"x": 558,
								"y": 1119,
								"width": 144,
								"height": 144
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1706596198"
					},
					{
						"id": 449,
						"name": "cemetery",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/cemetery.png",
							"frame": {
								"x": 560,
								"y": 694,
								"width": 152,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 539,
								"name": "cemPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/cemPic.png",
									"frame": {
										"x": 579,
										"y": 545,
										"width": 100,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790027894"
							},
							{
								"id": 504,
								"name": "checkCem",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkCem.png",
									"frame": {
										"x": 556,
										"y": 540,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790027984"
							}
						],
						"modification": "907137749"
					},
					{
						"id": 455,
						"name": "park",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/park.png",
							"frame": {
								"x": 345,
								"y": 694,
								"width": 71,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 541,
								"name": "parkPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/parkPic.png",
									"frame": {
										"x": 326,
										"y": 546,
										"width": 124,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790027988"
							},
							{
								"id": 507,
								"name": "checkP",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkP.png",
									"frame": {
										"x": 300,
										"y": 540,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790028079"
							}
						],
						"modification": "60101749"
					},
					{
						"id": 453,
						"name": "danger",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/danger.png",
							"frame": {
								"x": 77,
								"y": 694,
								"width": 113,
								"height": 34
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 537,
								"name": "dangerPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/dangerPic.png",
									"frame": {
										"x": 82,
										"y": 546,
										"width": 94,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790028083"
							},
							{
								"id": 510,
								"name": "checkD",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkD.png",
									"frame": {
										"x": 56,
										"y": 540,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057654"
							}
						],
						"modification": "1786257691"
					},
					{
						"id": 451,
						"name": "bench",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/bench.png",
							"frame": {
								"x": 77,
								"y": 978,
								"width": 96,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 543,
								"name": "benchPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/benchPic.png",
									"frame": {
										"x": 63,
										"y": 852,
										"width": 124,
										"height": 106
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057658"
							},
							{
								"id": 500,
								"name": "checkB",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkB.png",
									"frame": {
										"x": 52,
										"y": 835,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057748"
							}
						],
						"modification": "1034947106"
					},
					{
						"id": 457,
						"name": "shop",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/shop.png",
							"frame": {
								"x": 341,
								"y": 978,
								"width": 99,
								"height": 36
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 545,
								"name": "shopPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/shopPic.png",
									"frame": {
										"x": 314,
										"y": 834,
										"width": 124,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "57743800"
							},
							{
								"id": 519,
								"name": "checkS",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkS.png",
									"frame": {
										"x": 300,
										"y": 832,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057840"
							}
						],
						"modification": "865973669"
					},
					{
						"id": 447,
						"name": "fountain",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/fountain.png",
							"frame": {
								"x": 561,
								"y": 388,
								"width": 137,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 547,
								"name": "fountainPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/fountainPic.png",
									"frame": {
										"x": 565,
										"y": 241,
										"width": 124,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "57743803"
							},
							{
								"id": 498,
								"name": "checkF",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkF.png",
									"frame": {
										"x": 554,
										"y": 230,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057933"
							}
						],
						"modification": "1467792121"
					},
					{
						"id": 445,
						"name": "music",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/music.png",
							"frame": {
								"x": 343,
								"y": 388,
								"width": 95,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 549,
								"name": "guitar",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/guitar.png",
									"frame": {
										"x": 323,
										"y": 218,
										"width": 148,
										"height": 148
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790057937"
							},
							{
								"id": 516,
								"name": "checkM",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkM.png",
									"frame": {
										"x": 302,
										"y": 232,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790087507"
							}
						],
						"modification": "593435238"
					},
					{
						"id": 443,
						"name": "church",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/church.png",
							"frame": {
								"x": 76,
								"y": 388,
								"width": 111,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 551,
								"name": "churchPic",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/churchPic.png",
									"frame": {
										"x": 63,
										"y": 242,
										"width": 130,
										"height": 124
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790087511"
							},
							{
								"id": 513,
								"name": "checkCh",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkCh.png",
									"frame": {
										"x": 56,
										"y": 232,
										"width": 144,
										"height": 144
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790087601"
							}
						],
						"modification": "1541406135"
					},
					{
						"id": 6,
						"name": "Heading",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 31,
								"name": "featuresTab",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/featuresTab.png",
									"frame": {
										"x": 65,
										"y": 135,
										"width": 136,
										"height": 27
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790087627"
							},
							{
								"id": 10,
								"name": "appTitle",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/appTitle.png",
									"frame": {
										"x": 122,
										"y": 60,
										"width": 314,
										"height": 44
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1790087722"
							},
							{
								"id": 28,
								"name": "Settings",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Settings.png",
									"frame": {
										"x": 608,
										"y": 48,
										"width": 108,
										"height": 72
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1810196494"
							},
							{
								"id": 305,
								"name": "dividerTab",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/dividerTab.png",
									"frame": {
										"x": 0,
										"y": 173,
										"width": 750,
										"height": 6
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1810196497"
							},
							{
								"id": 495,
								"name": "prev2",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/prev2.png",
									"frame": {
										"x": 35,
										"y": 37,
										"width": 47,
										"height": 82
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1810196521"
							},
							{
								"id": 307,
								"name": "background",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/background.png",
									"frame": {
										"x": 0,
										"y": 0,
										"width": 750,
										"height": 242
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1810196524"
							}
						],
						"modification": "1462794633"
					}
				],
				"modification": "2021469008"
			},
			{
				"id": 533,
				"name": "deriveTime",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/deriveTime.png",
					"frame": {
						"x": 192,
						"y": 1160,
						"width": 338,
						"height": 38
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1789998254"
			},
			{
				"id": 467,
				"name": "BackgroundF",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundF.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 750,
						"height": 1331
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1810196529"
			}
		],
		"modification": "1630835540"
	}
]