window.__imported__ = window.__imported__ || {};
window.__imported__["people/layers.json.js"] = [
	{
		"id": 516,
		"name": "peopleAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 520,
				"name": "Notification",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/Notification.png",
					"frame": {
						"x": 0,
						"y": 1027,
						"width": 750,
						"height": 304
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "487362342"
			},
			{
				"id": 271,
				"name": "People",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 55,
						"name": "myProfile",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/myProfile.png",
							"frame": {
								"x": 65,
								"y": 196,
								"width": 411,
								"height": 142
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 494,
								"name": "notCheckedMe",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/notCheckedMe.png",
									"frame": {
										"x": 567,
										"y": 208,
										"width": 120,
										"height": 120
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "436609046"
							},
							{
								"id": 496,
								"name": "isCheckedMe",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/isCheckedMe.png",
									"frame": {
										"x": 568,
										"y": 210,
										"width": 117,
										"height": 116
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "436609049"
							}
						],
						"modification": "539767964"
					},
					{
						"id": 71,
						"name": "Curious",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Curious.png",
							"frame": {
								"x": 0,
								"y": 346,
								"width": 750,
								"height": 62
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 140,
								"name": "Aida",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Aida.png",
									"frame": {
										"x": 66,
										"y": 746,
										"width": 427,
										"height": 140
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 313,
										"name": "isCheckedAida",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/isCheckedAida.png",
											"frame": {
												"x": 567,
												"y": 756,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "436638714"
									},
									{
										"id": 315,
										"name": "notCheckedAida",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/notCheckedAida.png",
											"frame": {
												"x": 567,
												"y": 756,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "436638717"
									}
								],
								"modification": "768353748"
							},
							{
								"id": 100,
								"name": "Nardo",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Nardo.png",
									"frame": {
										"x": 16,
										"y": 581,
										"width": 718,
										"height": 155
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 317,
										"name": "isCheckedNardo",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/isCheckedNardo.png",
											"frame": {
												"x": 567,
												"y": 592,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "436638810"
									},
									{
										"id": 319,
										"name": "notCheckedNardo",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/notCheckedNardo.png",
											"frame": {
												"x": 567,
												"y": 592,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "436638813"
									}
								],
								"modification": "698753471"
							},
							{
								"id": 84,
								"name": "Berke",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/Berke.png",
									"frame": {
										"x": 16,
										"y": 417,
										"width": 718,
										"height": 155
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 321,
										"name": "isCheckedBerke",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/isCheckedBerke.png",
											"frame": {
												"x": 567,
												"y": 427,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "436638906"
									},
									{
										"id": 323,
										"name": "notCheckedBerke",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/notCheckedBerke.png",
											"frame": {
												"x": 567,
												"y": 427,
												"width": 120,
												"height": 120
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "437294022"
									}
								],
								"modification": "814282924"
							}
						],
						"modification": "885863299"
					},
					{
						"id": 144,
						"name": "Offline",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Offline.png",
							"frame": {
								"x": 0,
								"y": 899,
								"width": 750,
								"height": 62
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 170,
								"name": "A",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/A.png",
									"frame": {
										"x": 0,
										"y": 982,
										"width": 750,
										"height": 45
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 176,
										"name": "Arun",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/Arun.png",
											"frame": {
												"x": 30,
												"y": 1044,
												"width": 257,
												"height": 34
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "437294183"
									}
								],
								"modification": "217448276"
							},
							{
								"id": 165,
								"name": "C",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 750,
									"height": 1331
								},
								"maskFrame": null,
								"image": {
									"path": "images/C.png",
									"frame": {
										"x": 0,
										"y": 1098,
										"width": 750,
										"height": 46
									}
								},
								"imageType": "png",
								"children": [
									{
										"id": 174,
										"name": "Cesar",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/Cesar.png",
											"frame": {
												"x": 16,
												"y": 1162,
												"width": 718,
												"height": 56
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "2096135316"
									},
									{
										"id": 201,
										"name": "Chance",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/Chance.png",
											"frame": {
												"x": 16,
												"y": 1237,
												"width": 718,
												"height": 53
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "2013353146"
									},
									{
										"id": 153,
										"name": "Chris",
										"layerFrame": {
											"x": 0,
											"y": 0,
											"width": 750,
											"height": 1331
										},
										"maskFrame": null,
										"image": {
											"path": "images/Chris.png",
											"frame": {
												"x": 32,
												"y": 1310,
												"width": 208,
												"height": 21
											}
										},
										"imageType": "png",
										"children": [
											
										],
										"modification": "437323974"
									}
								],
								"modification": "1963616502"
							}
						],
						"modification": "2127096039"
					}
				],
				"modification": "488218688"
			},
			{
				"id": 6,
				"name": "Heading",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 43,
						"name": "activeTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/activeTab.png",
							"frame": {
								"x": 0,
								"y": 168,
								"width": 375,
								"height": 11
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437324065"
					},
					{
						"id": 473,
						"name": "Cat",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Cat.png",
							"frame": {
								"x": 458,
								"y": 139,
								"width": 133,
								"height": 28
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437324068"
					},
					{
						"id": 475,
						"name": "Peop",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/Peop.png",
							"frame": {
								"x": 150,
								"y": 139,
								"width": 83,
								"height": 28
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437353637"
					},
					{
						"id": 10,
						"name": "appTitle",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/appTitle.png",
							"frame": {
								"x": 36,
								"y": 60,
								"width": 314,
								"height": 44
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437353730"
					},
					{
						"id": 26,
						"name": "addFriends",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/addFriends.png",
							"frame": {
								"x": 424,
								"y": 42,
								"width": 144,
								"height": 78
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437413472"
					},
					{
						"id": 499,
						"name": "sendRequest",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/sendRequest.png",
							"frame": {
								"x": 632,
								"y": 54,
								"width": 60,
								"height": 60
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437442977"
					},
					{
						"id": 305,
						"name": "dividerTab",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/dividerTab.png",
							"frame": {
								"x": 0,
								"y": 173,
								"width": 750,
								"height": 6
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437442983"
					},
					{
						"id": 307,
						"name": "backgroundHeading",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 750,
							"height": 1331
						},
						"maskFrame": null,
						"image": {
							"path": "images/backgroundHeading.png",
							"frame": {
								"x": 0,
								"y": 0,
								"width": 750,
								"height": 188
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "437443007"
					}
				],
				"modification": "1783407526"
			},
			{
				"id": 425,
				"name": "BackgroundP",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1331
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundP.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 750,
						"height": 1331
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "437443011"
			}
		],
		"modification": "1637654392"
	}
]