window.__imported__ = window.__imported__ || {};
window.__imported__["title/layers.json.js"] = [
	{
		"id": 425,
		"name": "BackgroundT",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": {
			"path": "images/BackgroundT.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 750,
				"height": 1331
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1309440110"
	},
	{
		"id": 473,
		"name": "Start",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": {
			"path": "images/Start.png",
			"frame": {
				"x": 286,
				"y": 1088,
				"width": 157,
				"height": 53
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "234877719"
	},
	{
		"id": 480,
		"name": "mainPage",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1331
		},
		"maskFrame": null,
		"image": {
			"path": "images/mainPage.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 750,
				"height": 991
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "82930585"
	}
]