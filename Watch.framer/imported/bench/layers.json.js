window.__imported__ = window.__imported__ || {};
window.__imported__["bench/layers.json.js"] = [
	{
		"id": 61,
		"name": "destBenchAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 51,
				"name": "destBench",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 44,
						"name": "DeriveIn",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/DeriveIn.png",
							"frame": {
								"x": 1,
								"y": 16,
								"width": 332,
								"height": 363
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1586987210"
					}
				],
				"modification": "236479858"
			},
			{
				"id": 49,
				"name": "BackgroundNR",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundNR.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1617458109"
			}
		],
		"modification": "1867435866"
	}
]