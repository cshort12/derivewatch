window.__imported__ = window.__imported__ || {};
window.__imported__["destReached/layers.json.js"] = [
	{
		"id": 60,
		"name": "destReachedAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 51,
				"name": "destReached",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/destReached.png",
					"frame": {
						"x": 41,
						"y": 132,
						"width": 263,
						"height": 103
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 44,
						"name": "DeriveIn",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/DeriveIn.png",
							"frame": {
								"x": 36,
								"y": 17,
								"width": 255,
								"height": 23
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "590998658"
					}
				],
				"modification": "898520376"
			},
			{
				"id": 49,
				"name": "BackgroundNR",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundNR.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "179645798"
			}
		],
		"modification": "920983555"
	}
]