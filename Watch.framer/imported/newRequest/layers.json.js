window.__imported__ = window.__imported__ || {};
window.__imported__["newRequest/layers.json.js"] = [
	{
		"id": 23,
		"name": "newRequestAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 19,
				"name": "newRequest",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 10,
						"name": "Message",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/Message.png",
							"frame": {
								"x": 15,
								"y": 75,
								"width": 295,
								"height": 187
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1560737182"
					},
					{
						"id": 5,
						"name": "Notification",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/Notification.png",
							"frame": {
								"x": 15,
								"y": 11,
								"width": 201,
								"height": 29
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "2002626316"
					}
				],
				"modification": "1996918922"
			},
			{
				"id": 17,
				"name": "BackgroundN",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundN.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2001523925"
			}
		],
		"modification": "335475783"
	}
]