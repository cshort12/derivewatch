window.__imported__ = window.__imported__ || {};
window.__imported__["nextRoute/layers.json.js"] = [
	{
		"id": 57,
		"name": "nextRouteAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 51,
				"name": "nextRoute",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/nextRoute.png",
					"frame": {
						"x": 69,
						"y": 78,
						"width": 183,
						"height": 24
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 44,
						"name": "DeriveIn",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/DeriveIn.png",
							"frame": {
								"x": 65,
								"y": 16,
								"width": 212,
								"height": 24
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1921541351"
					},
					{
						"id": 36,
						"name": "walk",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/walk.png",
							"frame": {
								"x": 70,
								"y": 261,
								"width": 39,
								"height": 66
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1921451985"
					},
					{
						"id": 40,
						"name": "taxi",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/taxi.png",
							"frame": {
								"x": 218,
								"y": 273,
								"width": 54,
								"height": 54
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1921451982"
					},
					{
						"id": 38,
						"name": "bus",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/bus.png",
							"frame": {
								"x": 61,
								"y": 139,
								"width": 48,
								"height": 58
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1921451979"
					},
					{
						"id": 42,
						"name": "bike",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/bike.png",
							"frame": {
								"x": 209,
								"y": 139,
								"width": 72,
								"height": 63
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "215112344"
					}
				],
				"modification": "1442067243"
			},
			{
				"id": 49,
				"name": "BackgroundNR",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundNR.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1921451951"
			}
		],
		"modification": "1227962291"
	}
]