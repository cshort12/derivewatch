window.__imported__ = window.__imported__ || {};
window.__imported__["nextnext/layers.json.js"] = [
	{
		"id": 59,
		"name": "nextnextAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 51,
				"name": "nextnext",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/nextnext.png",
					"frame": {
						"x": 15,
						"y": 15,
						"width": 303,
						"height": 175
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 44,
						"name": "DeriveIn",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": null,
						"imageType": null,
						"children": [
							
						],
						"modification": "0"
					}
				],
				"modification": "100987122"
			},
			{
				"id": 49,
				"name": "BackgroundNR",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundNR.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1695745873"
			}
		],
		"modification": "1738363394"
	}
]