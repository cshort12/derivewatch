window.__imported__ = window.__imported__ || {};
window.__imported__["request/layers.json.js"] = [
	{
		"id": 46,
		"name": "requestAll",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 35,
				"name": "Request",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 26,
						"name": "deriveLater",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/deriveLater.png",
							"frame": {
								"x": 87,
								"y": 310,
								"width": 150,
								"height": 54
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1778252231"
					},
					{
						"id": 18,
						"name": "isGoing",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/isGoing.png",
							"frame": {
								"x": 177,
								"y": 143,
								"width": 128,
								"height": 128
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "515586713"
					},
					{
						"id": 20,
						"name": "notGoing",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/notGoing.png",
							"frame": {
								"x": 20,
								"y": 148,
								"width": 128,
								"height": 128
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "515586710"
					},
					{
						"id": 10,
						"name": "Info",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/Info.png",
							"frame": {
								"x": 6,
								"y": 17,
								"width": 297,
								"height": 101
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "335199472"
					}
				],
				"modification": "1789334008"
			},
			{
				"id": 33,
				"name": "BackgroundR",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/BackgroundR.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 333,
						"height": 379
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "515676142"
			}
		],
		"modification": "2107305818"
	}
]